﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Turizam.Models;

namespace Turizam
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        public void SaveAll()
        {
            while (true)
            {
                string userPath = AppDomain.CurrentDomain.BaseDirectory + @"\App_Data\Users.txt";
                using (StreamWriter streamWriter = new StreamWriter(userPath))
                {
                    foreach (var item in Korisnici.KorisniciList)
                    {
                        streamWriter.WriteLine(item.ID + ";" + item.KorisnickoIme + ";" + item.Lozinka + ";" + item.Ime + ";" + item.Prezime + ";" + item.Pol + ";" + item.Email + ";" + item.DatumRodjenja + ";" + item.IsDeleted + ";" + item.Uloga);
                    }
                }
                string arrangePath = AppDomain.CurrentDomain.BaseDirectory + @"\App_Data\Arranges.txt";
                using (StreamWriter streamWriter = new StreamWriter(arrangePath))
                {
                    foreach (var item in Aranzmani.AranzmaniList)
                    {
                        streamWriter.WriteLine(item.ID + ";" + item.Naziv + ";" + item.TipAranzmana + ";" + item.TipPrevoza + ";" + item.Lokacija + ";" + item.DatumPocetka + ";" + item.DatumZavrsetka + ";" + item.MestoNalazenja.Adresa + ";" + item.MestoNalazenja.GeogSirina + ";" + item.MestoNalazenja.GeogDuzina + ";" + item.VremeNalazenja + ";" + item.MaksimumPutnika + ";" + item.OpisAranzmana + ";" + item.ProgramPutovanja + ";" + item.PosterAranzmana + ";" + item.IsDeleted + ";" + item.ManagerName);
                    }
                }
                string smestajPath = AppDomain.CurrentDomain.BaseDirectory + @"\App_Data\Smestaji.txt";
                using (StreamWriter streamWriter = new StreamWriter(smestajPath))
                {
                    foreach (var item in Smestaji.SmestajiList)
                    {
                        streamWriter.WriteLine(item.ID + ";" + item.Naziv + ";" + item.TipSmestaja + ";" + item.Zvezdice + ";" + item.Bazen + ";" + item.Spa + ";" + item.HandicapFriendly + ";" + item.WiFi + ";" + item.IsDeleted + ";" + item.ArrangeID);
                    }
                }
                string smestajneJedPath = AppDomain.CurrentDomain.BaseDirectory + @"\App_Data\SmestajneJed.txt";
                using (StreamWriter streamWriter = new StreamWriter(smestajneJedPath))
                {
                    foreach (var item in SmestajneJedinice.SmestajneJediniceList)
                    {
                        streamWriter.WriteLine(item.ID + ";" + item.DozvoljenoGostiju + ";" + item.KucniLjubimci + ";" + item.Cena + ";" + item.Rezervisana + ";" + item.IsDeleted + ";" + item.SmestID);
                    }
                }
                string rezPath = AppDomain.CurrentDomain.BaseDirectory + @"\App_Data\Reservations.txt";
                using (StreamWriter streamWriter = new StreamWriter(rezPath))
                {
                    foreach (var item in Rezervacije.RezervacijeList)
                    {
                        streamWriter.WriteLine(item.ID + ";" + item.JID + ";" + item.Turista + ";" + item.Status + ";" + item.IzabranAranzman + ";" + item.IzabranaSmestajnaJedinica + ";" + item.IsDeleted);
                    }
                }
                Thread.Sleep(10000);
            }

        }
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            string userPath = AppDomain.CurrentDomain.BaseDirectory + @"\App_Data\Users.txt";
            using (StreamReader streamReader = new StreamReader(userPath))
            {
                string temp = streamReader.ReadLine();
                while (temp != null)
                {
                    string[] splitted = temp.Split(';');
                    Korisnici.AddKorisnikUloga(new Korisnik() { ID = int.Parse(splitted[0]), KorisnickoIme = splitted[1], Lozinka = splitted[2], Ime = splitted[3], Prezime = splitted[4], Pol = splitted[5], Email = splitted[6], DatumRodjenja = DateTime.Parse(splitted[7]), IsDeleted = bool.Parse(splitted[8]) }, splitted[9]);
                    temp = streamReader.ReadLine();
                }
            }
            string arrangePath = AppDomain.CurrentDomain.BaseDirectory + @"\App_Data\Arranges.txt";
            using (StreamReader streamReader = new StreamReader(arrangePath))
            {
                string temp = streamReader.ReadLine();
                while (temp != null)
                {
                    string[] splitted = temp.Split(';');
                    Aranzmani.AddAranzman(new Aranzman() { ID = int.Parse(splitted[0]), Naziv = splitted[1], TipAranzmana = splitted[2], TipPrevoza = splitted[3], Lokacija = splitted[4], DatumPocetka = DateTime.Parse(splitted[5]), DatumZavrsetka = DateTime.Parse(splitted[6]), MestoNalazenja = new MestoNalazenja() { Adresa = splitted[7], GeogSirina = double.Parse(splitted[8]), GeogDuzina = double.Parse(splitted[9]) }, VremeNalazenja = DateTime.Parse(splitted[10]), MaksimumPutnika = short.Parse(splitted[11]), OpisAranzmana = splitted[12], ProgramPutovanja = splitted[13], PosterAranzmana = splitted[14], IsDeleted = bool.Parse(splitted[15]) }, splitted[16]);
                    temp = streamReader.ReadLine();
                }
            }
            string smestajPath = AppDomain.CurrentDomain.BaseDirectory + @"\App_Data\Smestaji.txt";
            using (StreamReader streamReader = new StreamReader(smestajPath))
            {
                string temp = streamReader.ReadLine();
                while (temp != null)
                {
                    string[] splitted = temp.Split(';');
                    Smestaj thing = new Smestaj() { ID = int.Parse(splitted[0]), Naziv = splitted[1], TipSmestaja = splitted[2], Zvezdice = short.Parse(splitted[3]), Bazen = bool.Parse(splitted[4]), Spa = bool.Parse(splitted[5]), HandicapFriendly = bool.Parse(splitted[6]), WiFi = bool.Parse(splitted[7]), IsDeleted = bool.Parse(splitted[8]) };
                    Smestaji.AddSmestaj(thing, int.Parse(splitted[9]));
                    Aranzmani.FindByID(int.Parse(splitted[9])).Smestaj = thing;
                    temp = streamReader.ReadLine();
                }
            }
            string smestajneJedPath = AppDomain.CurrentDomain.BaseDirectory + @"\App_Data\SmestajneJed.txt";
            using (StreamReader streamReader = new StreamReader(smestajneJedPath))
            {
                string temp = streamReader.ReadLine();
                while (temp != null)
                {
                    string[] splitted = temp.Split(';');
                    SmestajneJedinice.AddSmestajnaJedinica(new SmestajnaJedinica() { ID = int.Parse(splitted[0]), DozvoljenoGostiju = short.Parse(splitted[1]), KucniLjubimci = bool.Parse(splitted[2]), Cena = double.Parse(splitted[3]), Rezervisana = bool.Parse(splitted[4]), IsDeleted = bool.Parse(splitted[5]) }, int.Parse(splitted[6]));
                    temp = streamReader.ReadLine();
                }
            }
            string rezPath = AppDomain.CurrentDomain.BaseDirectory + @"\App_Data\Reservations.txt";
            using (StreamReader streamReader = new StreamReader(rezPath))
            {
                string temp = streamReader.ReadLine();
                while (temp != null)
                {
                    string[] splitted = temp.Split(';');
                    Rezervacije.AddRezervacija(new Rezervacija() { ID = int.Parse(splitted[0]), JID = splitted[1], Turista = splitted[2], Status = splitted[3], IzabranAranzman = int.Parse(splitted[4]), IzabranaSmestajnaJedinica = int.Parse(splitted[5]), IsDeleted = bool.Parse(splitted[6]) });
                    temp = streamReader.ReadLine();
                }
            }


            new Thread(() => SaveAll()).Start();
        }
    }
}
