﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Turizam.Models;

namespace Turizam.Controllers
{
    public class SmestajneJediniceController : ApiController
    {
        public List<SmestajnaJedinica> Get()
        {
            return SmestajneJedinice.SmestajneJediniceList;
        }

        public SmestajnaJedinica GetByID(int ID)
        {
            return SmestajneJedinice.FindByID(ID);
        }
        [HttpPost]
        public IHttpActionResult Post(SmestajnaJedinica smestajnaJedinica)
        {
            return Ok(SmestajneJedinice.AddSmestajnaJedinica(smestajnaJedinica, smestajnaJedinica.SmestID));
        }

        public IHttpActionResult Put(SmestajnaJedinica smestajnaJedinica)
        {
            return Ok(SmestajneJedinice.UpdateSmestajnaJedinica(smestajnaJedinica));
        }

        public IHttpActionResult Delete(int ID)
        {
            SmestajnaJedinica temp = SmestajneJedinice.FindByID(ID);
            SmestajneJedinice.RemoveSmestajnaJedinica(temp);
            return StatusCode(System.Net.HttpStatusCode.NoContent);
        }
    }
}