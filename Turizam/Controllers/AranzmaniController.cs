﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Turizam.Models;

namespace Turizam.Controllers
{
    public class AranzmaniController : ApiController
    {
        public List<Aranzman> Get()
        {
            return Aranzmani.AranzmaniList;
        }

        public Aranzman GetByID(int ID)
        {
            return Aranzmani.FindByID(ID);
        }

        public IHttpActionResult Post(Aranzman aranzman)
        {
            return Ok(Aranzmani.AddAranzman(aranzman, aranzman.ManagerName));
        }

        public IHttpActionResult Put(Aranzman aranzman)
        {
            return Ok(Aranzmani.UpdateAranzman(aranzman));
        }

        [System.Web.Http.HttpDelete]
        public IHttpActionResult Delete(int ID)
        {
            Aranzman temp = Aranzmani.FindByID(ID);
            Aranzmani.RemoveAranzman(temp);
            return StatusCode(System.Net.HttpStatusCode.NoContent);
        }
    }
}
