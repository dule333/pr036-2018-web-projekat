﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Turizam.Models;

namespace Turizam.Controllers
{
    public class KorisniciController : ApiController
    {
        public List<Korisnik> Get()
        {
            return Korisnici.KorisniciList;
        }

        public Korisnik Get(string ID)
        {
            return Korisnici.FindByName(ID);
        }

        public IHttpActionResult Post(Korisnik korisnik)
        {
            if (korisnik == null)
                return BadRequest();
            if (korisnik.KorisnickoIme == null || korisnik.KorisnickoIme.Length < 3 || Korisnici.KorisniciList.Any(t => t.KorisnickoIme == korisnik.KorisnickoIme))
                return BadRequest();
            if (korisnik.Lozinka == null || korisnik.Lozinka.Length < 6)
                return BadRequest();
            return Ok(Korisnici.AddKorisnikUloga(korisnik, korisnik.Uloga));
        }

        public IHttpActionResult Put(Korisnik korisnik)
        {
            return Ok(Korisnici.UpdateKorisnik(korisnik));
        }
        public IHttpActionResult PutRezervacija(string id, Rezervacija rezervacija)
        {
            return Ok(Korisnici.UpdateKorisnikRezervacija(id, rezervacija));
        }

        public IHttpActionResult Delete(string ID)
        {
            Korisnik temp = Korisnici.FindByName(ID);
            Korisnici.RemoveKorisnik(temp);
            return StatusCode(System.Net.HttpStatusCode.NoContent);
        }
    }
}