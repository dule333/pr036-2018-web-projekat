﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Turizam.Models;

namespace Turizam.Controllers
{
    public class RezervacijeController : ApiController
    {
        public List<Rezervacija> Get()
        {
            return Rezervacije.RezervacijeList;
        }

        public Rezervacija GetByID(int ID)
        {
            return Rezervacije.FindByID(ID);
        }

        public IHttpActionResult Post(Rezervacija rezervacija)
        {
            if (rezervacija == null)
                return BadRequest();
            return Ok(Rezervacije.AddRezervacija(rezervacija));
        }

        public IHttpActionResult Put(Rezervacija rezervacija)
        {
            return Ok(Rezervacije.UpdateRezervacija(rezervacija));
        }

        public IHttpActionResult Delete(int ID)
        {
            Rezervacija temp = Rezervacije.FindByID(ID);
            Rezervacije.RemoveRezervacija(temp);
            return StatusCode(System.Net.HttpStatusCode.NoContent);
        }
    }
}