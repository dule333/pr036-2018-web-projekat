﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Turizam.Models;

namespace Turizam.Controllers
{
    public class SmestajiController : ApiController
    {
        public List<Smestaj> Get()
        {
            return Smestaji.SmestajiList;
        }

        public Smestaj GetByID(int ID)
        {
            return Smestaji.FindByID(ID);
        }

        public IHttpActionResult Post(Smestaj smestaj)
        {
            return Ok(Smestaji.AddSmestaj(smestaj, smestaj.ArrangeID));
        }

        public IHttpActionResult Put(Smestaj smestaj)
        {
            return Ok(Smestaji.UpdateSmestaj(smestaj));
        }

        [HttpDelete]
        public IHttpActionResult Delete(int ID)
        {
            Smestaj temp = Smestaji.FindByID(ID);
            Smestaji.RemoveSmestaj(temp);
            return StatusCode(System.Net.HttpStatusCode.NoContent);
        }
    }
}