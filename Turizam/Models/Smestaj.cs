﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Turizam.Models
{
    public class Smestaj
    {
        public int ID { get; set; }
        public string TipSmestaja { get; set; }
        public string Naziv { get; set; }
        public short Zvezdice { get; set; }
        public bool Bazen { get; set; }
        public bool Spa { get; set; }
        public bool HandicapFriendly { get; set; }
        public bool WiFi { get; set; }
        public List<SmestajnaJedinica> SmestajneJedinice { get; set; }
        public int ArrangeID { get; set; }
        public bool IsDeleted { get; set; }
    }
}