﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Turizam.Models
{
    public class Aranzman
    {
        public int ID { get; set; }
        public string Naziv { get; set; }
        public string TipAranzmana { get; set; }
        public string TipPrevoza { get; set; }
        public string Lokacija { get; set; }
        public DateTime DatumPocetka { get; set; }
        public DateTime DatumZavrsetka { get; set; }
        public MestoNalazenja MestoNalazenja { get; set; }
        public DateTime VremeNalazenja { get; set; }
        public short MaksimumPutnika { get; set; }
        public string OpisAranzmana { get; set; }
        public string ProgramPutovanja { get; set; }
        public string PosterAranzmana { get; set; }
        public Smestaj Smestaj { get; set; }
        public string ManagerName { get; set; }
        public bool IsDeleted { get; set; }
    }
}