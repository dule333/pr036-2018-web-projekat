﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Turizam.Models
{
    public class MestoNalazenja
    {
        public string Adresa { get; set; }
        public double GeogDuzina { get; set; }
        public double GeogSirina { get; set; }
    }
}