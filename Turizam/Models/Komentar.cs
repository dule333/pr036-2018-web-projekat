﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Turizam.Models
{
    public class Komentar
    {
        public Korisnik Komentator { get; set; }
        public Aranzman Aranzman { get; set; }
        public string Tekst { get; set; }
        public short Ocena { get; set; }
        public bool Approved { get; set; }
        public bool IsDeleted { get; set; }
    }
}