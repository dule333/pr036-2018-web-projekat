﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Turizam.Models
{
    public class Korisnici
    {
        public static List<Korisnik> KorisniciList { get; set; } = new List<Korisnik>();
        public static Korisnik FindByName(string ID)
        {
            return KorisniciList.Find(item => item.KorisnickoIme == ID);
        }

        public static Korisnik AddKorisnik(Korisnik korisnik)
        {
            korisnik.ID = GenerateID();
            korisnik.Uloga = "korisnik";
            korisnik.Rezervacije = new List<Rezervacija>();
            korisnik.KreiraniAranzmani = new List<Aranzman>();
            KorisniciList.Add(korisnik);
            return korisnik;
        }
        public static Korisnik AddKorisnikUloga(Korisnik korisnik, string uloga)
        {
            korisnik.ID = GenerateID();
            korisnik.Uloga = uloga;
            korisnik.Rezervacije = new List<Rezervacija>();
            korisnik.KreiraniAranzmani = new List<Aranzman>();
            KorisniciList.Add(korisnik);
            return korisnik;
        }

        public static void RemoveKorisnik(Korisnik korisnik)
        {
            FindByName(korisnik.KorisnickoIme).IsDeleted = true;
        }
        public static Korisnik UpdateKorisnik(Korisnik korisnik)
        {
            Korisnik postojeci = FindByName(korisnik.KorisnickoIme);
            postojeci.Ime = korisnik.Ime;
            postojeci.Prezime = korisnik.Prezime;
            postojeci.DatumRodjenja = korisnik.DatumRodjenja;
            postojeci.Pol = korisnik.Pol;
            postojeci.Uloga = korisnik.Uloga;
            postojeci.Email = korisnik.Email;
            postojeci.Lozinka = korisnik.Lozinka;
            postojeci.Rezervacije = postojeci.Rezervacije;
            postojeci.KreiraniAranzmani = korisnik.KreiraniAranzmani;
            return postojeci;
        }

        public static Korisnik UpdateKorisnikRezervacija(string id, Rezervacija rezervacija)
        {
            Korisnik korisnik = FindByName(id);
            korisnik.Rezervacije.Add(rezervacija);
            return korisnik;
        }

        private static int GenerateID()
        {
            return Math.Abs(Guid.NewGuid().GetHashCode());
        }
    }
}