﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Turizam.Models
{
    public class Aranzmani
    {
        public static List<Aranzman> AranzmaniList { get; set; } =
            new List<Aranzman>();
        public static Aranzman FindByID(int ID)
        {
            return AranzmaniList.Find(item => item.ID == ID);
        }

        public static Aranzman AddAranzman(Aranzman aranzman, string manID)
        {
            if(aranzman.ID == -1)
                aranzman.ID = GenerateID();
            aranzman.ManagerName = manID;
            AranzmaniList.Add(aranzman);
            Korisnici.FindByName(manID).KreiraniAranzmani.Add(aranzman);
            return aranzman;
        }

        public static void RemoveAranzman(Aranzman aranzman)
        {
            FindByID(aranzman.ID).IsDeleted = true;
        }
        public static Aranzman UpdateAranzman(Aranzman Aranzman)
        {
            Aranzman postojeci = FindByID(Aranzman.ID);
            postojeci.Naziv = Aranzman.Naziv;
            postojeci.TipAranzmana = Aranzman.TipAranzmana;
            postojeci.TipPrevoza = Aranzman.TipPrevoza;
            postojeci.Lokacija = Aranzman.Lokacija;
            postojeci.DatumPocetka = Aranzman.DatumPocetka;
            postojeci.DatumZavrsetka = Aranzman.DatumZavrsetka;
            postojeci.MestoNalazenja = Aranzman.MestoNalazenja;
            postojeci.VremeNalazenja = Aranzman.VremeNalazenja;
            postojeci.MaksimumPutnika = Aranzman.MaksimumPutnika;
            postojeci.OpisAranzmana = Aranzman.OpisAranzmana;
            postojeci.ProgramPutovanja = Aranzman.ProgramPutovanja;
            postojeci.PosterAranzmana = Aranzman.PosterAranzmana;
            postojeci.Smestaj = Aranzman.Smestaj;
            return postojeci;
        }
        private static int GenerateID()
        {
            return Math.Abs(Guid.NewGuid().GetHashCode());
        }
    }
}