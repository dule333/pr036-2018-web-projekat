﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Turizam.Models
{
    public class SmestajneJedinice
    {
        public static List<SmestajnaJedinica> SmestajneJediniceList { get; set; } = new List<SmestajnaJedinica>();
        public static SmestajnaJedinica FindByID(int ID)
        {
            return SmestajneJediniceList.Find(item => item.ID == ID);
        }
        public static void Zauzmi(int ID)
        {
            FindByID(ID).Rezervisana = true;
        }
        public static void Oslobodi(int ID)
        {
            FindByID(ID).Rezervisana = false;
        }

        public static SmestajnaJedinica AddSmestajnaJedinica(SmestajnaJedinica smestajnaJedinica, int smestID)
        {
            if(smestajnaJedinica.ID == -1)
                smestajnaJedinica.ID = GenerateID();
            smestajnaJedinica.SmestID = smestID;
            smestajnaJedinica.Rezervisana = false;
            SmestajneJediniceList.Add(smestajnaJedinica);
            if (Smestaji.FindByID(smestID).SmestajneJedinice == null)
                Smestaji.FindByID(smestID).SmestajneJedinice = new List<SmestajnaJedinica>();
            Smestaji.FindByID(smestID).SmestajneJedinice.Add(smestajnaJedinica);
            return smestajnaJedinica;
        }

        public static void RemoveSmestajnaJedinica(SmestajnaJedinica smestajnaJedinica)
        {
            FindByID(smestajnaJedinica.ID).IsDeleted = true;
        }
        public static SmestajnaJedinica UpdateSmestajnaJedinica(SmestajnaJedinica smestajnaJedinica)
        {
            SmestajnaJedinica postojeci = FindByID(smestajnaJedinica.ID);
            postojeci.Cena = smestajnaJedinica.Cena;
            postojeci.KucniLjubimci = smestajnaJedinica.KucniLjubimci;
            postojeci.DozvoljenoGostiju = smestajnaJedinica.DozvoljenoGostiju;
            return postojeci;
        }
        private static int GenerateID()
        {
            return Math.Abs(Guid.NewGuid().GetHashCode());
        }
    }
}