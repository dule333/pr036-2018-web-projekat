﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Turizam.Models
{
    public class Korisnik
    {
        public int ID { get; set; }
        public string KorisnickoIme { get; set; }
        public string Lozinka { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Pol { get; set; }
        public string Email { get; set; }
        public DateTime DatumRodjenja { get; set; }
        public string Uloga { get; set; }
        public List<Rezervacija> Rezervacije { get; set; }
        public List<Aranzman> KreiraniAranzmani { get; set; }
        public bool IsDeleted { get; set; }
    }
}