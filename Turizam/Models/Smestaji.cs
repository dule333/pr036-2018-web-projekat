﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Turizam.Models
{
    public class Smestaji
    {
        public static List<Smestaj> SmestajiList { get; set; } = new List<Smestaj>();
        public static Smestaj FindByID(int ID)
        {
            return SmestajiList.Find(item => item.ID == ID);
        }

        public static Smestaj AddSmestaj(Smestaj smestaj, int arrID)
        {
            smestaj.ArrangeID = arrID;
            if(smestaj.ID == -1)
            smestaj.ID = GenerateID();
            SmestajiList.Add(smestaj);
            if(Aranzmani.FindByID(arrID) != null)
                Aranzmani.FindByID(arrID).Smestaj = smestaj;
            return smestaj;
        }

        public static void RemoveSmestaj(Smestaj smestaj)
        {
            FindByID(smestaj.ID).IsDeleted = true;
        }
        public static Smestaj UpdateSmestaj(Smestaj Smestaj)
        {
            Smestaj postojeci = FindByID(Smestaj.ID);
            postojeci.Naziv = Smestaj.Naziv;
            postojeci.TipSmestaja = Smestaj.TipSmestaja;
            postojeci.Zvezdice = Smestaj.Zvezdice;
            postojeci.Bazen = Smestaj.Bazen;
            postojeci.Spa = Smestaj.Spa;
            postojeci.HandicapFriendly = Smestaj.HandicapFriendly;
            postojeci.WiFi = Smestaj.WiFi;
            postojeci.SmestajneJedinice = Smestaj.SmestajneJedinice;
            return postojeci;
        }

        private static int GenerateID()
        {
            return Math.Abs(Guid.NewGuid().GetHashCode());
        }
    }
}