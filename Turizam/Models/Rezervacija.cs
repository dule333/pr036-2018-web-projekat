﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Turizam.Models
{
    public class Rezervacija
    {
        public int ID { get; set; }
        public string JID { get; set; }
        public string Turista { get; set; }
        public string Status { get; set; }
        public int IzabranAranzman { get; set; }
        public int IzabranaSmestajnaJedinica { get; set; }
        public bool IsDeleted { get; set; }
    }
}