﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Turizam.Models
{
    public class Rezervacije
    {
        public static List<Rezervacija> RezervacijeList { get; set; } = new List<Rezervacija>();
        public static Rezervacija FindByID(int ID)
        {
            return RezervacijeList.Find(item => item.ID == ID);
        }

        public static Rezervacija AddRezervacija(Rezervacija rezervacija)
        {
            if(rezervacija.ID <= 0)
                rezervacija.ID = GenerateID();
            rezervacija.JID = (rezervacija.ID.ToString() + rezervacija.ID.ToString()).ToString().Substring(0, 15);
            RezervacijeList.Add(rezervacija);
            Korisnici.UpdateKorisnikRezervacija(rezervacija.Turista, rezervacija);
            SmestajneJedinice.Zauzmi(rezervacija.IzabranaSmestajnaJedinica);
            return rezervacija;
        }

        public static void RemoveRezervacija(Rezervacija rezervacija)
        {
            FindByID(rezervacija.ID).IsDeleted = true;
        }
        public static Rezervacija UpdateRezervacija(Rezervacija rezervacija)
        {
            Rezervacija postojeci = FindByID(rezervacija.ID);
            postojeci.Status = rezervacija.Status;
            Korisnici.FindByName(postojeci.Turista).Rezervacije.Find(temp => temp.ID == rezervacija.ID).Status = rezervacija.Status;
            SmestajneJedinice.Oslobodi(postojeci.IzabranaSmestajnaJedinica);
            return postojeci;
        }

        private static int GenerateID()
        {
            return Math.Abs(Guid.NewGuid().GetHashCode());
        }
    }
}