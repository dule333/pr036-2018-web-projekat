﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Turizam.Models
{
    public class SmestajnaJedinica
    {
        public int ID { get; set; }
        public int SmestID { get; set; }
        public short DozvoljenoGostiju { get; set; }
        public bool KucniLjubimci { get; set; }
        public double Cena { get; set; }
        public bool Rezervisana { get; set; }
        public bool IsDeleted { get; set; }
    }
}